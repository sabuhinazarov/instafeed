const express = require('express');
const app = express();
const cheerio = require('cheerio');
const request = require('request');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
    next();
});

app.get('/', function(req, res) {
    res.render('pages/index');
});

app.get('/getfeed/:username', function (req, res, next) {
    const url = "https://www.instagram.com/" + req.params.username;
    request(url, function(error, response, html){
        const $ = cheerio.load(html);
        var window = "";
        const jsonData = $('script').eq(2).html();
        var json = eval(jsonData);

        var instaimages = json.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges;
        res.render('pages/widget', { instaimages: instaimages });
    });
});

app.get('/getjson/:username', function (req, res, next) {
    const url = "https://www.instagram.com/" + req.params.username;
    request(url, function(error, response, html){
        const $ = cheerio.load(html);
        var window = "";
        const jsonData = $('script').eq(2).html();
        var json = eval(jsonData);

        var instaimages = json.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges;
        res.status(200).json({
           images: instaimages
        });
    });
});

const port = 6111;
app.listen(process.env.PORT || port);
console.log("server started on port: " + port);